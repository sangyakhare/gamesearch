package com.ql.assessment.gamesearch.model;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @Author : Sangya Khare
 */

public class Game implements Serializable {
    public static final String IMAGE_BASE_URL = "https://images.igdb.com/igdb/image/upload/t_thumb/";
    public static final String PNG = ".png";
    //parse screenshots
    @SerializedName("screenshots")
    List<ScreenShot> screenShots;

    @SerializedName("name")
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Log.d("pojp", "name =" +name);
        this.name = name;
    }


    public List<ScreenShot> getScreenShots() {
        return screenShots;
    }

    public void setScreenShots(List<ScreenShot> screenShots) {
        this.screenShots = screenShots;
    }

    public class ScreenShot implements Serializable{
        @SerializedName("image_id")
        String imageId;

        public String getImageId() {
            return imageId;
        }

        public void setImageId(String imageId) {
            this.imageId = imageId;
        }

        public String getImageUrl(){
            return  IMAGE_BASE_URL + getImageId()+ PNG;
        }
    }
}
