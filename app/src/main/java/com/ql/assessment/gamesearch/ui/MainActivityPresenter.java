package com.ql.assessment.gamesearch.ui;

import com.ql.assessment.gamesearch.model.Game;
import com.ql.assessment.gamesearch.servicelayer.GameSearchServiceSdk;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Activity Presenter
 * @Author: Sangya Khare
 */
class MainActivityPresenter {

    private final View view;

    interface View{
        void showProgressBar();

        void updateGamesList(List<Game> gameList);

        void hideProgressBar();

        void showNoResultMessage();

        void hideNoResultMessage();

        void showServiceFailedError();
    }

    public MainActivityPresenter(View view){
        this.view = view;
    }

    public void callGameSearchService(String text) {
        if(text.length() > 0) {
            GameSearchServiceSdk sdk = new GameSearchServiceSdk();
            view.showProgressBar();
            sdk.searchGames(text).enqueue(new Callback<List<Game>>() {
                @Override
                public void onResponse(Call<List<Game>> call, Response<List<Game>> response) {
                    view.updateGamesList(response.body());
                    view.hideProgressBar();
                    if (response.isSuccessful()) {
                        if (response.body().size() == 0) {
                            view.showNoResultMessage();
                        } else {
                            view.hideNoResultMessage();
                        }
                    } else {
                        view.showServiceFailedError();
                    }
                }
                @Override
                public void onFailure(Call<List<Game>> call, Throwable t) {
                    view.showServiceFailedError();
                }
            });
        }
    }
}
