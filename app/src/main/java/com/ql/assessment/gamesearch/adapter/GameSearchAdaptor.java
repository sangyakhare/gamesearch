package com.ql.assessment.gamesearch.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ql.assessment.gamesearch.R;
import com.ql.assessment.gamesearch.model.Game;

import java.util.List;

/**
 * Game Adaptor
 * @Author: Sangya Khare
 */
public class GameSearchAdaptor extends ArrayAdapter<Game> {

    public GameSearchAdaptor(@NonNull Context context, List<Game> gamesList) {
        super(context, 0, gamesList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Game game = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_game, parent, false);
        }
        TextView gameNameTextView = convertView.findViewById(R.id.nameTextView);
        ImageView imageView =  convertView.findViewById(R.id.imageView);
        gameNameTextView.setText(game.getName());
        if(game.getScreenShots() != null && game.getScreenShots().size() > 0)
            Glide.with(parent.getContext()).load(game.getScreenShots().get(0).getImageUrl()).into(imageView);
        return convertView;
    }

}
