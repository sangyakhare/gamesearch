package com.ql.assessment.gamesearch.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ql.assessment.gamesearch.R;
import com.ql.assessment.gamesearch.adapter.GameSearchAdaptor;
import com.ql.assessment.gamesearch.model.Game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Main  Activity
 * @Author: Sangya Khare
 */
public class MainActivity extends AppCompatActivity implements MainActivityPresenter.View {

    private EditText searchEditText;
    private ListView listView;
    private GameSearchAdaptor gameSearchAdapter;
    private List<Game> gamesList = new ArrayList<>();
    private ProgressBar progressBar;
    private TextView messagetextView;

    private MainActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);
        searchEditText = findViewById(R.id.editText);
        progressBar = findViewById(R.id.progressBar);
        messagetextView = findViewById(R.id.messageTextView);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                gameSearchAdapter.clear();
                hideNoResultMessage();
                presenter.callGameSearchService(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        gameSearchAdapter = new GameSearchAdaptor(getApplicationContext(), gamesList);
        listView.setAdapter(gameSearchAdapter);


    }

    @Override
    public void onStart(){
        super.onStart();
        presenter = new MainActivityPresenter(this);
    }
    @Override
    public void hideNoResultMessage() {
        messagetextView.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showServiceFailedError() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
        alertBuilder.setMessage(getString(R.string.service_error));
        alertBuilder.setCancelable(true);

        alertBuilder.setPositiveButton(
                getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @Override
    public void showNoResultMessage() {
        messagetextView.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateGamesList(List<Game> gameList) {
        gamesList.clear();
        gamesList.addAll(gameList);
        gameSearchAdapter.notifyDataSetChanged();
    }


}
