package com.ql.assessment.gamesearch.servicelayer;

import com.ql.assessment.gamesearch.model.Game;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * @Author: Sangya Khare
 */
public class GameSearchServiceSdk {
    private GameSearchService getService(){
        return RetrofitClient.getClient().create(GameSearchService.class);
    }

    public Call<List<Game>> searchGames(String searchText){
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"),"fields name, screenshots.image_id; search \""
                +searchText+"\"; limit 50;");
        return getService().searchGames(requestBody);
    }
}
