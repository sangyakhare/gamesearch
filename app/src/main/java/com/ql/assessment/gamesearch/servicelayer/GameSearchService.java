package com.ql.assessment.gamesearch.servicelayer;

import com.ql.assessment.gamesearch.model.Game;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Service Interface
 * @Author: Sangya Khare
 */

public interface GameSearchService {
    @POST("/games")
    @Headers("user-key:078d79e5f6cc18cd0e45b0580dbfef44")
    Call<List<Game>> searchGames(@Body RequestBody body);
}
